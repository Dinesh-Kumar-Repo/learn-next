import BlogList from "./blog-list/page";
import "./page.css";

export default function Home() {
  return (
    <main className="container">
      <BlogList />
    </main>
  );
}

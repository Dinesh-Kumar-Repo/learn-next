import Link from 'next/link';
import style from "./Blog.module.css";

export default function Custom404() {
    return (
        <div className={style.notFound}>
            <h1 className={style.h1}>404</h1>
            <h2 className={style.h2}>Page Not Found</h2>
            <p className={style.p}>Sorry, the page you are looking for does not exist.</p>
            <div className={style.homeLink}>
            <Link href="/" style={{textDecoration: 'none'}}>
               <span className={style.homeLink}> Go to Home</span>
            </Link>
            </div>
        </div>
    );
}

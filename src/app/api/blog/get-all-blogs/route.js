import connectToDB from "../../../../database/index";
import Blog from "../../../../models/blog";
import { NextResponse } from "next/server";

export async function GET() {
  try {
    await connectToDB();

    const allBlogs = await Blog.find({}); 

    return NextResponse.json({
      success: true,
      data: allBlogs,
    });
  } catch (e) {
    console.log(e);

    return NextResponse.json({
      success: false,
      message: "Failed to fetch blogs",
    });
  }
}

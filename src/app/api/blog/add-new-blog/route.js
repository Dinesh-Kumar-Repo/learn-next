// import Blog from "@/models/blog";
// import { NextResponse } from "next/server";

// export async function POST(req) {
//   try {
//     await connectToDB();
//     const { extrectData } = await req.json();

//     const newlyCreatedBlogData = await Blog.create(extrectData);

//     if (newlyCreatedBlogData) {
//       return NextResponse.json({
//         success: true,
//         message: "Failed To Add New Data",
//       });
//     } else {
//       return NextResponse.json({
//         success: false,
//         message: "Something went wrong",
//       });
//     }
//   } catch (e) {
//     console.log(e);

//     return NextResponse.json({
//       success: false,
//       message: "Something went wrong",
//     });
//   }
// }

import connectToDB from "../../../../database/index";
import Blog from "../../../../models/blog";
import { NextResponse } from "next/server";

export async function POST(req) {
  try {
    await connectToDB();
    const extrectData = await req.json();

    const newlyCreatedBlogData = await Blog.create(extrectData);

    if (newlyCreatedBlogData) {
      return NextResponse.json({
        success: true,
        message: "Blog added successfully",
      });
    } else {
      return NextResponse.json({
        success: false,
        message: "Failed to add new data",
      });
    }
  } catch (e) {
    console.log(e);

    return NextResponse.json({
      success: false,
      message: "Something went wrong",
    });
  }
}

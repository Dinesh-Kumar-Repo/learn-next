import connectToDB from '../../../../database';
import { ObjectId } from 'mongodb';

export async function PUT(req) {
  const body = await req.json();
  const { id, title, description } = body;

  if (!id || !title || !description) {
    return new Response(JSON.stringify({ message: 'Missing required fields' }), {
      status: 400,
      headers: { 'Content-Type': 'application/json' }
    });
  }

  try {
    const db = (await connectToDB()).connection.db;
    const result = await db.collection('blogs').updateOne(
      { _id: new ObjectId(id) },
      { $set: { title, description, updatedAt: new Date() } }
    );

    if (result.modifiedCount === 0) {
      return new Response(JSON.stringify({ message: 'Blog not found' }), {
        status: 404,
        headers: { 'Content-Type': 'application/json' }
      });
    }

    return new Response(JSON.stringify({ success: true, message: 'Blog updated successfully' }), {
      status: 200,
      headers: { 'Content-Type': 'application/json' }
    });
  } catch (error) {
    return new Response(JSON.stringify({ message: 'Internal Server Error', error: error.message }), {
      status: 500,
      headers: { 'Content-Type': 'application/json' }
    });
  }
}

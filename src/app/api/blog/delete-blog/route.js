
import connectToDB from "@/database";
import { NextResponse } from "next/server";
import Blog from "@/models/blog"; 

export async function DELETE(req) {
  try {
    await connectToDB();

    const { searchParams } = new URL(req.url, `http://${req.headers.host}`);
    const currentBlogId = searchParams.get("id");

    if (!currentBlogId) {
      return NextResponse.json({
        success: false,
        message: "Blog ID is required",
      });
    }

    const result = await Blog.findByIdAndDelete(currentBlogId);

    if (!result) {
      return NextResponse.json({
        success: false,
        message: "Blog not found or already deleted",
      });
    }

    return NextResponse.json({
      success: true,
      message: "Blog deleted successfully",
    });

  } catch (e) {
    console.log(e);

    return NextResponse.json({
      success: false,
      message: "Something went wrong",
    });
  }
}

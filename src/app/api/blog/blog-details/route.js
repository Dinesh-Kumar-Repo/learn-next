import connectToDB from "@/database";
import Blog from "@/models/blog";
import { NextResponse } from "next/server";

export async function GET(req) {
  try {
    await connectToDB();
    const { searchParams } = new URL(req.url);
    const currentBlogID = searchParams.get("id");

    if (!currentBlogID) {
      return NextResponse.json({
        success: false,
        message: "ID is Required",
      });
    }

    const getBlogDetails = await Blog.find({ _id: currentBlogID });

    if (getBlogDetails && getBlogDetails.length) {
      return NextResponse.json({
        success: true,
        message: getBlogDetails[0],
      });
    }
  } catch (e) {
    console.log(e);

    return NextResponse.json({
      success: false,
      message: "Something went wrong",
    });
  }
}

"use client";

import { useState } from "react";
import { useRouter } from "next/navigation";
import styles from "../Blog.module.css";
import Link from "next/link";

const initialFormData = {
  title: "",
  description: "",
};

const initialErrorData = {
  title: "",
  description: "",
};

export default function AddBlog() {
  const [blogFormData, setBlogFormData] = useState(initialFormData);
  const [errorData, setErrorData] = useState(initialErrorData);
  const [responseMessage, setResponseMessage] = useState("");
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  async function handleAddBlog(e) {
    e.preventDefault();
    const errors = {};

    if (!blogFormData.title.trim()) {
      errors.title = "Title is required";
    }

    if (!blogFormData.description.trim()) {
      errors.description = "Description is required";
    }

    if (Object.keys(errors).length > 0) {
      setErrorData(errors);
      return;
    }

    setLoading(true);

    const response = await fetch("/api/blog/add-new-blog", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(blogFormData),
    });
    const result = await response.json();

    setLoading(false);
    setResponseMessage(result.message);

    if (result.success) {
      setBlogFormData(initialFormData);
      setErrorData(initialErrorData); 

      setTimeout(() => {
        setResponseMessage("");
        router.push("/");
      }, 1000);
    }
  }

  function handleReset(e) {
    e.preventDefault();
    setBlogFormData(initialFormData);
    setErrorData(initialErrorData); 
  }

  return (
    <div className={styles.container}>
      <h1 className={styles.header}>
        <strong>Add Blog</strong>
      </h1>
      <form className={styles.form} onSubmit={handleAddBlog}>
        <div className={styles.formGroup}>
          <label className={styles.label}>Enter Blog Title :</label>
          <input
            className={`${styles.input} ${errorData.title ? styles.errorInput : ''}`}
            name="title"
            value={blogFormData.title}
            onChange={(e) =>
              setBlogFormData({
                ...blogFormData,
                title: e.target.value,
              })
            }
            placeholder="Enter Blog Title"
          />
          {errorData.title && (
            <p className={styles.errorMessage}>{errorData.title}</p>
          )}
        </div>
        <div className={styles.formGroup}>
          <label className={styles.label}>Enter Blog Description :</label>
          <textarea
            className={`${styles.textarea} ${errorData.description ? styles.errorInput : ''}`}
            name="description"
            value={blogFormData.description}
            onChange={(e) =>
              setBlogFormData({
                ...blogFormData,
                description: e.target.value,
              })
            }
            placeholder="Enter Blog Description"
          />
          {errorData.description && (
            <p className={styles.errorMessage}>{errorData.description}</p>
          )}
        </div>
        <div className={styles.formGroup}>
          <button
            className={styles.button}
            type="submit"
            disabled={loading}
          >
            {loading ? "Wait ..." : "Add Blog +"} 
          </button>
          <button
            className={styles.buttonreset}
            type="button"
            onClick={handleReset}
          >
            Reset
          </button>
        </div>
        <div className={styles.links}>
          <Link href="/">Go to Home Page</Link>
        </div>
      </form>
      {responseMessage && (
        <p className={styles.message}>{responseMessage}</p>
      )}
    </div>
  );
}

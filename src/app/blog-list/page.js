"use client";

import { useState, useEffect } from "react";
import { useRouter } from "next/navigation";
import Loading from "./loading";
import styles from "../Blog.module.css";

export default function BlogList() {
  const [blogs, setBlogs] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  const [confirmDialog, setConfirmDialog] = useState(null);
  const router = useRouter();

  useEffect(() => {
    async function fetchBlogs() {
      try {
        const response = await fetch("/api/blog/get-all-blogs");
        const data = await response.json();

        if (response.ok && data.success) {
          setBlogs(data.data || []);
        } else {
          setError(data.message || "Failed to fetch blogs");
        }
      } catch (err) {
        setError("An error occurred while fetching blogs");
      } finally {
        setLoading(false);
      }
    }

    fetchBlogs();
  }, []);

  const handleEdit = (id) => {
    router.push(`/blog-list/${id}`);
  };

  const handleDelete = (id) => {
    setConfirmDialog(id);
  };

  const handleConfirmDelete = async (id) => {
    try {
      const response = await fetch(`/api/blog/delete-blog?id=${id}`, {
        method: "DELETE",
      });
      const data = await response.json();

      if (response.ok && data.success) {
        setBlogs(blogs.filter(blog => blog._id !== id)); // Update blogs state after deletion
      } else {
        setError(data.message || "Failed to delete blog");
      }
    } catch (err) {
      setError("An error occurred while deleting blog");
    } finally {
      setConfirmDialog(null);
    }
  };

  const handleCancelDelete = () => {
    setConfirmDialog(null);
  };

  const formatDate = (dateString) => {
    const options = { day: "2-digit", month: "2-digit", year: "numeric" };
    return new Date(dateString).toLocaleDateString("en-GB", options);
  };

  const handleAddBlog = () => {
    router.push("/add-blog");
  };

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <div className={styles.error}>{error}</div>;
  }

  return (
    <div className={styles.containers}>
      <h1 className={styles.header}>
        <strong>All Blogs</strong>
      </h1>
      <div className={styles.textcenter}>
        <button className={styles.addButton} onClick={handleAddBlog}>
          Add New Blog +
        </button>
      </div>
      <div className={styles.blogList}>
        {blogs.length === 0 ? (
          <p>No blogs available</p>
        ) : (
          blogs.map((blog) => (
            <div key={blog._id} className={styles.blogItem}>
              <h2 className={styles.blogTitle}>{blog.title}</h2>
              <p className={styles.blogDescription}>{blog.description}</p>
              <p className={styles.blogDate}>
                Created at: {formatDate(blog.createdAt)}
              </p>
              <div className={styles.buttonContainers}>
                <button
                  className={styles.editButton}
                  onClick={() => handleEdit(blog._id)}
                >
                  Edit
                </button>
                <button
                  className={styles.deleteButton}
                  onClick={() => handleDelete(blog._id)}
                >
                  Delete
                </button>
              </div>
            </div>
          ))
        )}
      </div>

      {confirmDialog && (
        <div className={styles.confirmDialog}>
          <p style={{ color: 'red' }}><strong>Are you sure you want to delete this blog?</strong></p>
          <div className={styles.confirmButtons}>
            <button
              className={styles.confirmYes}
              onClick={() => handleConfirmDelete(confirmDialog)}
            >
              Yes
            </button>
            &nbsp; &nbsp;
            <button className={styles.confirmNo} onClick={handleCancelDelete}>
              No
            </button>
          </div>
        </div>
      )}
    </div>
  );
}

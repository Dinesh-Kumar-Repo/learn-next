"use client";

import { useState, useEffect } from 'react';
import styles from '../../Blog.module.css'; 
import { useRouter } from 'next/navigation';

export default function BlogDetailsComponents({ blogDetails }) {
  const [blogFormData, setBlogFormData] = useState({
    title: '',
    description: ''
  });
  const [errorData, setErrorData] = useState({});
  const [loading, setLoading] = useState(false);
  const [responseMessage, setResponseMessage] = useState('');
  const router = useRouter();

  
  useEffect(() => {
    if (blogDetails) {
      setBlogFormData({
        title: blogDetails.title,
        description: blogDetails.description
      });
    }
  }, [blogDetails]);

  const handleAddBlog = async (e) => {
    e.preventDefault();
    setLoading(true);

    try {
      const response = await fetch('/api/blog/update-blog', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id: blogDetails._id,
          title: blogFormData.title,
          description: blogFormData.description
        })
      });

      const data = await response.json();
      if (response.ok) {
        setResponseMessage('Blog updated successfully!');
        setTimeout(() => {
          router.push('/');
        }, 2000);
      } else {
        setResponseMessage(data.message || 'Failed to update blog');
      }
    } catch (error) {
      setResponseMessage('An error occurred while updating the blog');
    } finally {
      setLoading(false);
    }
  };

  const handleCancel = () => {
    router.push("/");
  };

  return (
    <div className={styles.container}>
      <h1 className={styles.header}>
        <strong>Edit Blog</strong>
      </h1>
      <form className={styles.form} onSubmit={handleAddBlog}>
        <div className={styles.formGroup}>
          <label className={styles.label}>Enter Blog Title :</label>
          <input
            className={`${styles.input} ${errorData.title ? styles.errorInput : ''}`}
            name="title"
            value={blogFormData.title}
            onChange={(e) =>
              setBlogFormData({
                ...blogFormData,
                title: e.target.value,
              })
            }
            placeholder="Enter Blog Title"
          />
          {errorData.title && (
            <p className={styles.errorMessage}>{errorData.title}</p>
          )}
        </div>
        <div className={styles.formGroup}>
          <label className={styles.label}>Enter Blog Description :</label>
          <textarea
            className={`${styles.textarea} ${errorData.description ? styles.errorInput : ''}`}
            name="description"
            value={blogFormData.description}
            onChange={(e) =>
              setBlogFormData({
                ...blogFormData,
                description: e.target.value,
              })
            }
            placeholder="Enter Blog Description"
          />
          {errorData.description && (
            <p className={styles.errorMessage}>{errorData.description}</p>
          )}
        </div>
        <div className={styles.formGroup}>
          <button
            className={styles.button}
            type="submit"
            disabled={loading}
          >
            {loading ? "Wait ..." : "Save Changes"} 
          </button>
          <button
            className={styles.buttonreset}
            type="button"
            onClick={handleCancel}
          >
            Cancel
          </button>
        </div>
      </form>
      {responseMessage && (
        <p className={styles.message}>{responseMessage}</p>
      )}
    </div>
  );
}

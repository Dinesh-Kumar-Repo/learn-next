import BlogDetailsComponents from "../details/index";

async function fetchBlogItemDetails(getCurrentBlogID) {
  if (!getCurrentBlogID) {
    throw new Error('Invalid blog ID');
  }

  // console.log(`Fetching details for blog ID: ${getCurrentBlogID}`);

  const req = await fetch(
    `http://localhost:3000/api/blog/blog-details?id=${getCurrentBlogID}`,
    {
      method: "GET",
      cache: "no-store",
    }
  );

  const data = await req.json();

  // console.log('API response:', data);

  if (data?.success) {
    // console.log('Data fetched successfully:', data.message);
    return data.message; // Use the correct field
  }
  
  throw new Error('Failed to fetch blog details');
}

export default async function BlockDetail({ params }) {
  const { detail } = params; 

  if (!detail) {
    return <div>Error: Missing blog ID</div>;
  }

  try {
    const blogDetails = await fetchBlogItemDetails(detail);

    // console.log('Blog details:', blogDetails);
    return (
      <div>
        <BlogDetailsComponents blogDetails={blogDetails} />
      </div>
    );
  } catch (error) {
    console.error('Error fetching blog details:', error);
    return <div style={{textAlign: 'center', color: 'red', marginTop: '45px'}}><strong>Error fetching blog details</strong></div>;
  }
}


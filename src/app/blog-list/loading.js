import "./loading.css";
export default function Page() {
  return (
    <div className="loader-container">
      <div className="loader-line"></div>
    </div>
  );
}

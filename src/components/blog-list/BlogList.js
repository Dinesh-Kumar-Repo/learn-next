"use client";

import { useEffect, useState } from "react";
import styles from "../Blog.module.css";

export default function BlogList() {
  const [blogs, setBlogs] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");

  useEffect(() => {
    async function fetchBlogs() {
      try {
        const response = await fetch("/api/blog/get-all-blogs");
        const data = await response.json();

        if (response.ok) {
          setBlogs(data.blogs); // Adjust this according to the actual response structure
        } else {
          setError(data.message || "Failed to fetch blogs");
        }
      } catch (err) {
        setError("An error occurred while fetching blogs");
      } finally {
        setLoading(false);
      }
    }

    fetchBlogs();
  }, []);

  if (loading) {
    return <div className={styles.loading}>Loading...</div>;
  }

  if (error) {
    return <div className={styles.error}>{error}</div>;
  }

  return (
    <div className={styles.container}>
      <h1 className={styles.header}>All Blogs</h1>
      <div className={styles.blogList}>
        {blogs.length === 0 ? (
          <p>No blogs available</p>
        ) : (
          blogs.map((blog) => (
            <div key={blog.id} className={styles.blogItem}>
              <h2 className={styles.blogTitle}>{blog.title}</h2>
              <p className={styles.blogDescription}>{blog.description}</p>
            </div>
          ))
        )}
      </div>
    </div>
  );
}

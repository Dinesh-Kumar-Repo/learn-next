// import mongoose from "mongoose";

// const BlogSchema = new mongoose.Schema(
//   {
//     title: String,
//     description: String,
//   },
//   { timestamps: true }
// );

// const Blog = mongoose.models.Blog || mongoose.models("Blogs", BlogSchema);

// export default Blog;
import mongoose from "mongoose";

const BlogSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

const Blog = mongoose.models.Blog || mongoose.model("Blog", BlogSchema);

export default Blog;

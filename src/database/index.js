// import mongoose, { connect } from "mongoose";

// const conectToDB = async () => {
//   mongoose
//     .connect(
//       "mongodb+srv://dineshkumardevelopers:dineshkumardeveloperspassword@cluster0.yaq4l5l.mongodb.net/"
//     )
//     .then(() => console.log("Database Connect Successfully"))
//     .catch((e) => console.log(e));
// };

// export default conectToDB;

import mongoose from "mongoose";

const MONGO_URI = process.env.MONGO_URI || "mongodb+srv://dineshkumardevelopers:dineshkumardeveloperspassword@cluster0.yaq4l5l.mongodb.net/";

if (!MONGO_URI) {
  throw new Error("Please provide a valid MongoDB URI.");
}

let cached = global.mongoose;

if (!cached) {
  cached = global.mongoose = { conn: null, promise: null };
}

const connectToDB = async () => {
  if (cached.conn) {
    return cached.conn;
  }

  if (!cached.promise) {
    cached.promise = mongoose.connect(MONGO_URI).then((mongoose) => {
      console.log("Database Connected Successfully");
      return mongoose;
    }).catch((error) => {
      console.error("Database Connection Error:", error);
      throw error;
    });
  }
  cached.conn = await cached.promise;
  return cached.conn;
};

export default connectToDB;
